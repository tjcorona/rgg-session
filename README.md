# RGG Session

RGG is an open-source tool to generate several types nuclear reactor
assembly/core geometry and mesh. RGG is a part of SIGMA’s MeshKit Mesh
Generation Toolkit. It can generate solid models of core assemblies as
well as tetrahedral and hexahedral meshes of the reactor core. The RGG
Session is an SMTK modeling session designed to interface with SIGMA's
reactor core modeling tools.

This project is a port of the original RGG Session written by Haocheng Liu
<haocheng.liu@kitware.com> in SMTK, which in turn was ported from the
RGG GUI developed jointly by Kitware and ANL.
