set(library_name "_smtkPybindRGGSession")
set(module_path "rggsession")
set(build_path "${PROJECT_BINARY_DIR}/${module_path}")
set(install_path "${PYTHON_MODULEDIR}/${module_path}")

pybind11_add_module(${library_name} PybindRGGSession.cxx)
target_include_directories(${library_name} PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  )
target_link_libraries(${library_name} LINK_PUBLIC smtkCore smtkRGGSession)
set_target_properties(${library_name}
  PROPERTIES
    CXX_VISIBILITY_PRESET hidden
    COMPILE_FLAGS ${PYBIND11_FLAGS}
    LIBRARY_OUTPUT_DIRECTORY "${build_path}"
  )

# Install library
install(TARGETS ${library_name} DESTINATION "${install_path}")

# Create and install module __init__.py
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/__init__.py.in"
  "${build_path}/__init__.py" @ONLY
  )

install(
  FILES "${build_path}/__init__.py"
  DESTINATION "${install_path}"
  )
