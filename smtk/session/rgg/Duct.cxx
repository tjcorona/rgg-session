//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/Duct.h"
#include "smtk/session/rgg/json/jsonDuct.h"

namespace smtk
{
namespace session
{
namespace rgg
{
using json = nlohmann::json;
std::set<std::string> Duct::s_usedLabels;

struct Duct::Internal
{
  Internal()
  {
    this->name = "Duct0";
    this->cutAway = 0;
  }

  std::string name;
  bool cutAway;
  std::vector<Segment> segments;
};

Duct::Duct() : m_internal(std::make_shared<Internal>())
{
}

Duct::Duct(const std::string& name, bool cutAway) : m_internal(std::make_shared<Internal>())
{
  this->m_internal->name = name;
  this->m_internal->cutAway = cutAway;
}

Duct::~Duct()
{
}

const std::string& Duct::name() const
{
  return m_internal->name;
}
bool Duct::isCutAway() const
{
  return m_internal->cutAway;
}
std::vector<Duct::Segment>& Duct::segments()
{
  return m_internal->segments;
}
const std::vector<Duct::Segment>& Duct::segments() const
{
  return m_internal->segments;
}

void Duct::setName(const std::string& name)
{
  m_internal->name = name;
}

void Duct::setCutAway(bool cutAway)
{
  m_internal->cutAway = cutAway;
}

void Duct::setSegments(const std::vector<Segment>& segments)
{
  m_internal->segments = segments;
}

bool Duct::operator==(const Duct& other) const
{
  json thisJ = *this;
  json otherJ = other;
  return thisJ == otherJ;
}

bool Duct::operator!=(const Duct& other) const
{
  return !(*this == other);
}

} // namespace rgg
} //namespace session
} // namespace smtk
