//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/json/jsonDuct.h"

namespace smtk
{
namespace session
{
namespace rgg
{
using json = nlohmann::json;

void to_json(json& j, const Duct::Segment& segment)
{
  j["baseZ"] = segment.baseZ;
  j["height"] = segment.height;
  j["layers"] = segment.layers;
}

void from_json(const json& j, Duct::Segment& segment)
{
  using layer = std::tuple<int, double, double>;
  try
  {
    segment.baseZ = j.at("baseZ");
    segment.height = j.at("height");
    std::vector<layer> layers = j.at("layers");
    segment.layers.swap(layers);

  }
  catch(json::exception &e)
  {
    std::cerr << e.what() << "\n";
    std::cerr << "Failed to parse a segment in the duct" <<std::endl;
  }
}

void to_json(json& j, const Duct& duct)
{
  j["name"] = duct.name();
  j["cutAway"] = duct.isCutAway();
  j["segments"] = duct.segments();
}

void from_json(const json& j, Duct& duct)
{
  try
  {
    duct.setName(j.at("name"));
    duct.setCutAway(j.at("cutAway"));
    duct.setSegments(j.at("segments"));
  }
  catch(json::exception &e)
  {
    std::cerr << e.what() << "\n";
    std::cerr << "Failed to parse the duct" <<std::endl;
  }
}

} // namespace rgg
} // namespace session
} // namespace smtk
