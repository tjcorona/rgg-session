cmake_minimum_required(VERSION 3.8.2)

#-----------------------------------------------------------------------------
# Plugin name and version

project(rgg-session VERSION 1.0)

#-----------------------------------------------------------------------------
# This plugin requires C++11

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_EXTENSIONS False)

#-----------------------------------------------------------------------------
# Plugin options

option(ENABLE_TESTING "Enable Testing" OFF)
option(ENABLE_PYTHON_WRAPPING "Build Python Wrappings" ON)

#-----------------------------------------------------------------------------
# Find packages for the plugin
find_package(smtk REQUIRED)
find_package(ParaView REQUIRED)
find_package(Boost 1.64.0 REQUIRED COMPONENTS filesystem)
find_package(Qt5 REQUIRED COMPONENTS Core)

if (ENABLE_PYTHON_WRAPPING)
  # Only look for python packages if they are not already found (this is a
  # workaround for pybind11's custom python cmake scripts)
  if (NOT PYTHONINTERP_FOUND)
    find_package(PythonInterp 2.7 REQUIRED)
  endif ()
  if (NOT PYTHONLIBS_FOUND)
    find_package(PythonLibs REQUIRED)
  endif ()

  # Set the python module extension (needed for pybind11)
  if(MSVC)
    set(PYTHON_MODULE_EXTENSION ".pyd")
  else()
    set(PYTHON_MODULE_EXTENSION ".so")
  endif()
  set(PYTHON_MODULE_PREFIX "")

  find_package(pybind11 REQUIRED)
endif ()

#-----------------------------------------------------------------------------
# SMTK settings

# We need the path to SMTK's include directory so EncodeStringFunctions.cmake
# finds things.
get_target_property(SMTK_INCLUDE_DIR smtkCore INTERFACE_INCLUDE_DIRECTORIES)

# smtk's included CMake macros
include(SMTKMacros)
include(SMTKOperationXML)

#-----------------------------------------------------------------------------
# ParaView settings

# ParaView's included CMake macros
include(${PARAVIEW_USE_FILE})
include(ParaViewPlugins)

#-----------------------------------------------------------------------------
# Boost settings

add_library(boost_cxx_flags INTERFACE)
# For MSVC:
#  (/EHsc) setup windows exception handling
#  (/wd4996) quiet warnings about printf being potentially unsafe
#  (/wd4503) quiet warnings about truncating decorated name
#  (-DBOOST_ALL_NO_LIB) remove autolinking
target_compile_options(boost_cxx_flags INTERFACE
  $<$<CXX_COMPILER_ID:MSVC>:/EHsc /wd4996 /wd4503 -DBOOST_ALL_NO_LIB>)

set_property(TARGET Boost::boost APPEND PROPERTY INTERFACE_LINK_LIBRARIES boost_cxx_flags)

#-----------------------------------------------------------------------------
# Python settings

# Initialize PYTHON_MODULEDIR.
# This stores the location where we'll install our Python modules.
# Note that PYTHON_MODULEDIR may be provided to override this behavior.
if (NOT DEFINED PYTHON_MODULEDIR)
  if (INSTALL_PYTHON_TO_SITE_PACKAGES)
    execute_process(
      COMMAND
      ${PYTHON_EXECUTABLE}
      -c "import site; print(site.getsitepackages())[-1]"
      RESULT_VARIABLE PYTHON_MODULEDIR
      )
  elseif(WIN32)
    set(PYTHON_MODULEDIR
      "bin/Lib/site-packages")
  else()
    set(PYTHON_MODULEDIR
      "lib/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages")
  endif()
endif()

#-----------------------------------------------------------------------------
# Pybind11 settings

set(PYBIND11_FLAGS " ")
if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
    CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
    CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  set(PYBIND11_FLAGS "${PYBIND11_FLAGS} -Wno-shadow")
endif()

#-----------------------------------------------------------------------------
# CMake build settings

# Setting this ensures that "make install" will leave rpaths to external
# libraries (not part of the build-tree e.g. Qt, ffmpeg, etc.) intact on
# "make install". This ensures that one can install a version of ParaView on the
# build machine without any issues. If this not desired, simply specify
# CMAKE_INSTALL_RPATH_USE_LINK_PATH when configuring and "make install" will
# strip all rpaths, which is default behavior.
if (NOT CMAKE_INSTALL_RPATH_USE_LINK_PATH)
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif ()

# Set the directory where the binaries will be stored
set(EXECUTABLE_OUTPUT_PATH         ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

# Add our Cmake directory to the module search path
list(APPEND CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/CMake)

#-----------------------------------------------------------------------------
# Plugin tests

if (ENABLE_TESTING)
  enable_testing()
  include(TestingMacros)
endif()

#-----------------------------------------------------------------------------
# Plugin sources

# Include the plugin's source directory
add_subdirectory(smtk)

#-----------------------------------------------------------------------------
# Plugin configuration

include(CMakePackageConfigHelpers)

# Our requirements for a version file are basic, so we use CMake's basic version
# file generator
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
  VERSION ${${PROJECT_NAME}_VERSION}
  COMPATIBILITY AnyNewerVersion
)

# Export the targets generated by the plugin
export(EXPORT RGGSession
  FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Targets.cmake"
)

# As an SMTK plugin-generating project, consuming this project at configure-time
# should append this project's plugins to the global list of SMTK plugins
get_property(SMTK_PLUGINS GLOBAL PROPERTY SMTK_PLUGINS)
file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Config.cmake"
  "include(\"\${CMAKE_CURRENT_LIST_DIR}/${PROJECT_NAME}Targets.cmake\")\n"
  "list(APPEND\ SMTK_PLUGINS\ ${SMTK_PLUGINS})"
  )

# Install the generated targets file
set(ConfigPackageLocation lib/cmake/${PROJECT_NAME})
install(EXPORT RGGSession
  FILE
    ${PROJECT_NAME}Targets.cmake
  DESTINATION
    ${ConfigPackageLocation}
)

# Install the generated configure files
install(
  FILES
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Config.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
  COMPONENT
    Devel
)
